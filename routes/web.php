<?php

use App\Http\Controllers\{
    HomeController,
    MoviesController,
    DashboardController,
};
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/movies/{id}', [MoviesController::class, 'show'])->name('movies.show'); 

Route::group([
    'middleware' => [
        'auth:sanctum',
        config('jetstream.auth_session'),
        'verified',
    ],
    'prefix' => 'dashboard',
    'as' => 'dashboard.',
], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('index');
    Route::get('/movies', [DashboardController::class, 'getMovies'])->name('movies');
    Route::get('/movies/refetch', [DashboardController::class, 'refetchMovies'])->name('movies.refetch');
    Route::get('/movies/{id}', [DashboardController::class, 'deleteMovie'])->name('movies.delete');
    Route::get('/movies/{id}/edit', [DashboardController::class, 'editMovie'])->name('movies.edit');
    Route::post('/movies/{id}/update', [DashboardController::class, 'updateMovie'])->name('movies.update');
});
