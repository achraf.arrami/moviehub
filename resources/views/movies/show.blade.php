{{-- movie details page --}}
<x-guest-layout>
    <x-navbar />
    <div class="container max-w-screen-xl mx-auto">
        <nav class=" flex py-4 px-6 " aria-label="Breadcrumb">
            <ol class="inline-flex items-center space-x-1 md:space-x-2 rtl:space-x-reverse">
                <li class="inline-flex items-center">
                    <a href="{{ route('home') }}"
                        class="inline-flex items-center text-sm font-medium text-gray-700 hover:text-blue-600 dark:text-gray-400 dark:hover:text-white">
                        <svg class="w-3 h-3 me-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor" viewBox="0 0 20 20">
                            <path
                                d="m19.707 9.293-2-2-7-7a1 1 0 0 0-1.414 0l-7 7-2 2a1 1 0 0 0 1.414 1.414L2 10.414V18a2 2 0 0 0 2 2h3a1 1 0 0 0 1-1v-4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v4a1 1 0 0 0 1 1h3a2 2 0 0 0 2-2v-7.586l.293.293a1 1 0 0 0 1.414-1.414Z" />
                        </svg>
                        Home
                    </a>
                </li>
                <li>
                    <div class="flex items-center">
                        <svg class="rtl:rotate-180 w-3 h-3 text-gray-400 mx-1" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="m1 9 4-4-4-4" />
                        </svg>
                        <span class="ms-1 text-sm font-medium text-gray-700 md:ms-2">
                            {{ $movie->title }}
                        </span>
                    </div>
                </li>
        </nav>
    </div>
    <div class="container max-w-screen-xl mx-auto px-4 py-16">

        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <div class="mx-auto lg:col-span-1">
                <img src="{{ 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/' . $movie->image }}" alt="poster"
                    class="w-[400px] rounded-lg shadow-lg">
            </div>
            <div class="mx-auto lg:col-span-2 px-6 md:px-0">
                <h2 class="text-2xl font-bold text-gray-800 dark:text-gray-100">{{ $movie->title }}</h2>
                <div class="flex items-center space-x-2 mt-4">
                    <div class="flex items-center">
                        <svg class="w-4 h-4 text-yellow-300 me-1" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor" viewBox="0 0 22 20">
                            <path
                                d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
                        </svg>
                        <span class="text-gray-600 dark:text-gray-300">{{ round($movie->vote_average, 1) }} / 10</span>
                    </div>
                    <span>&bull;</span>
                    <span>{{ \Carbon\Carbon::parse($movie->release_date)->format('M d, Y') }}</span>
                    <span>&bull;</span>
                    <span>{{ $movie->runtime > 60 ? floor($movie->runtime / 60) . 'h ' . $movie->runtime % 60 . 'min' : $movie->runtime . 'min' }}</span>
                </div>
                <div class="flex items-center space-x-2 mt-4">
                    @foreach ($movie->categories as $category)
                        <span class="text-gray-600 dark:text-gray-300">{{ $category->name }}</span>
                    @endforeach
                </div>
                <p class="text-gray-600 dark:text-gray-300 mt-8">
                    {!! nl2br($movie->overview) !!}
                </p>
                <div class="mt-8">
                    <a href="{{ $movie->homepage }}" target="_blank"
                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        Visit Homepage
                        <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9" />
                        </svg>
                    </a>
                </div>
                <h3 class="text-xl font-bold text-gray-800 dark:text-gray-100 mt-8">Production Companies</h3>
                <div class="flex flex-wrap gap-4 p-5 border-t border-gray-200 dark:border-gray-700 mt-5">
                    @foreach ($movie->production_companies as $company)
                        @if ($company->logo_path)
                            <img src="https://www.themoviedb.org/t/p/w92/{{ $company->logo_path }}"
                                alt="{{ $company->name }}" title="{{ $company->name }}" class="h-8">
                        @else
                            <span class="text-gray-600 dark:text-gray-300">{{ $company->name }}</span>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
