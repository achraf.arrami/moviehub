<x-guest-layout>
    @push('styles')
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    @endpush
    <x-navbar />
    <form class="container max-w-lg mx-auto mt-6" action="{{ route('home') }}" method="GET">
        <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
        <div class="flex space-y-2 flex-col">
            <div class="relative flex-1">
                <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                    </svg>
                </div>
                <input type="search" id="default-search" name="title" value="{{ request('title') }}"
                    class="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
                    placeholder="Search for movie name" />

            </div>
            <select name="categories[]" multiple="multiple"
                class="js-example-basic-multiple flex-1 bg-gray-50 border-gray-300 h-[54px]"
                placeholder="Select categories">
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}" @selected(in_array($category->id, request('categories', [])))>
                        {{ $category->name }}
                    </option>
                @endforeach
            </select>
            <button type="submit"
                class="mx-auto text-white bg-blue-700 hover:bg-blue-800 font-medium rounded-lg text-sm px-4 py-2 block w-[180px]">
                Search
            </button>
        </div>

    </form>
    <div class="container max-w-screen-xl mx-auto px-4 py-16">
        <h2 class="text-2xl font-bold text-gray-800 dark:text-gray-100">Trending Movies</h2>

        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-8 mt-8">
            @forelse ($movies as $movie)
                <x-movie-card :movie="$movie" />
            @empty
                <p class="text-gray-600 dark:text-gray-300">No movies found</p>
            @endforelse
        </div>
        <div class="mt-8">
            {{ $movies->links() }}
        </div>
    
    </div>
    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.7.1.min.js"
            integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.js-example-basic-multiple').select2({
                    placeholder: 'Select categories',
                });
            });
        </script>
    @endpush

 
</x-guest-layout>
