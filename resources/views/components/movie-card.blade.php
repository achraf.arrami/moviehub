<div
    class="max-w-[300px] bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mx-auto">
    <a href="{{ route('movies.show', $movie->movie_id) }}">
        <img class="rounded-t-lg" src="https://media.themoviedb.org/t/p/w300_and_h450_bestv2/{{ $movie->image }}"
            alt="{{ isset($movie->title) ? $movie->title : $movie->name }}" loading="lazy" />
    </a>
    <div class="p-5 ">
        <div>
            <a href="{{ route('movies.show', $movie->movie_id) }}">
                <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white min-h-16">
                    {{ isset($movie->title) ? $movie->title : $movie->name }}
                </h5>
            </a>
            <div class="flex items-center">
                <svg class="w-4 h-4 text-yellow-300 me-1" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor" viewBox="0 0 22 20">
                    <path
                        d="M20.924 7.625a1.523 1.523 0 0 0-1.238-1.044l-5.051-.734-2.259-4.577a1.534 1.534 0 0 0-2.752 0L7.365 5.847l-5.051.734A1.535 1.535 0 0 0 1.463 9.2l3.656 3.563-.863 5.031a1.532 1.532 0 0 0 2.226 1.616L11 17.033l4.518 2.375a1.534 1.534 0 0 0 2.226-1.617l-.863-5.03L20.537 9.2a1.523 1.523 0 0 0 .387-1.575Z" />
                </svg>
                <span class="text-gray-600 dark:text-gray-300">{{ round($movie->vote_average, 1) }} / 10</span>
            </div>

            <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 line-clamp-3">
                {{ $movie->overview }}
            </p>
            <div>
                {{ implode(' | ', $movie->categories->pluck('name')->toArray()) }}
            </div>
            <a href="{{ route('movies.show', $movie->movie_id) }}"
                class="mt-2 block inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                View Details
                <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                    fill="none" viewBox="0 0 14 10">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M1 5h12m0 0L9 1m4 4L9 9" />
                </svg>
            </a>
        </div>
    </div>

</div>
