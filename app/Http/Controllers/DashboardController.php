<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use App\Models\Category;
use Illuminate\Support\Facades\Artisan;


class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard.index');
    }

    public function getMovies()
    {
        $movies = Movie::all();

        return view('dashboard.movies', [
            'movies' => $movies
        ]);
    }

    public function deleteMovie($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->delete();

        return redirect()->route('dashboard.movies');
    }

    public function editMovie($id)
    {
        $movie = Movie::findOrFail($id)->load('categories');
        $categories = Category::all();

        return view('dashboard.editMovie', [
            'movie' => $movie,
            'categories' => $categories
        ]);
    }

    public function updateMovie(Request $request, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->title = $request->title;
        $movie->overview = $request->overview;
        $movie->categories()->sync($request->categories);
        $movie->save();

        return redirect()->route('dashboard.movies');
    }

    public function refetchMovies()
    {
        Artisan::call('movies:store-trending');

        return redirect()->route('dashboard.movies');
    }
}
