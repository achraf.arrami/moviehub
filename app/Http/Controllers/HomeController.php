<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use Illuminate\Support\Facades\Artisan;
use App\Models\Category;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();
        if (Movie::count() == 0) {
            Artisan::call('movies:store-trending');
        }

        $title = $request->input('title');
        $selectedCategories = $request->input('categories');
        $movies = Movie::where('title', 'like', "%$title%")
            ->when($selectedCategories, function ($query, $selectedCategories) {
                return $query->whereHas('categories', function ($query) use ($selectedCategories) {
                    $query->whereIn('category_id', $selectedCategories);
                });
            })->paginate(8);


        return view('home', ['movies' => $movies, 'categories' => $categories]);
    }

    public function search(Request $request)
    {
        $title = $request->input('title');
        $movies = Movie::where('title', 'like', "%$title%")->get();

        return view('home', ['movies' => $movies]);
    }

}
