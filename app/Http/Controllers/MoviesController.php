<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Movie;

class MoviesController extends Controller
{
    public function show($id)
    {

        $movie = Movie::where('movie_id', $id)->with('categories')->first();

        $_movie = $this->getMovieById($id);
        $movie->production_companies = isset($_movie->production_companies) ? $_movie->production_companies : [];
        $movie->homepage = isset($_movie->homepage) ? $_movie->homepage : '#';

        return view('movies.show', ['movie' => $movie])->layout('layouts.guest');

    }

    public function getMovieById($id)
    {
        try {
            $API_KEY = env('THEMOVIEDB_API_KEY');
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $API_KEY,
            ])->get('https://api.themoviedb.org/3/movie/' . $id . '?language=en-US');

            $data = $response->object();

            return $data;
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }
}
