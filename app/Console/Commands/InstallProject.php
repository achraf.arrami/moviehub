<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
class InstallProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:install';

    /**
     * The console command description.
     *
     * @var string
     */
   

    protected $description = 'Install the project';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        if (!file_exists('.env')) {
            $this->info('Creating .env file...');
            if (DIRECTORY_SEPARATOR == '/') {
                $process = new Process(['cp', '.env.example', '.env']);
            }else{
                $process = new Process(['copy', '.env.example', '.env']);
            }
            $process->run();
        }

        $this->info('Generating application key...');
        $this->call('key:generate');


     

        $this->info('Installing project dependencies...');
        $process = new Process(['npm', 'i']);
        $process->run();

        if (!$process->isSuccessful()) {
            $this->error('Failed to install npm dependencies: ' . $process->getErrorOutput());
            return;
        }

        $this->info('Building assets...');
        $process = new Process(['npm', 'run', 'build']);
        $process->run();

        if (!$process->isSuccessful()) {
            $this->error('Failed to build assets: ' . $process->getErrorOutput());
            return;
        }


        $this->info('Project installation complete. please make sure to add THEMOVIEDB_API_KEY in .env file');
        
    }
}
