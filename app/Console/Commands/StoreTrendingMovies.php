<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Movie;
use App\Models\Category;

class StoreTrendingMovies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'movies:store-trending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store trending movies and categories from external API';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->storeCategories();
        $this->storeTrendingMovie();
    }

    public function storeTrendingMovie()
    {
        $trendingMovies = $this->getTrendingMovies();
        $movies = $trendingMovies->results;
        
        Movie::query()->delete();
        foreach ($movies as $movie) {
            $_movie = new Movie([
                'movie_id' => $movie->id,
                'title' => isset($movie->title) ? $movie->title : $movie->name,
                'vote_average' => $movie->vote_average,
                'release_date' => isset($movie->release_date) ? $movie->release_date : $movie->first_air_date,
                'overview' => $movie->overview,
                'image' => $movie->poster_path,
            ]);
            $_movie->save();
            // store the categories
            $this->storeMovieCategories($movie->genre_ids, $_movie->id);
        }

    }

    public function storeMovieCategories($genre_ids, $movie_id)
    {
        foreach ($genre_ids as $genre_id) {
            $category = Category::where('genre_id', $genre_id)->first();
            if ($category) {
                $category->movies()->attach($movie_id);
            }
        }
    }



    public function storeCategories()
    {
        $categories = $this->getCategories();
        $genres = $categories->genres;
        Category::query()->delete();


        foreach ($genres as $genre) {
            $category = new Category([
                'genre_id' => $genre->id,
                'name' => $genre->name,
            ]);
            $category->save();
        }

    }

    public function getCategories()
    {
        $API_KEY = env('THEMOVIEDB_API_KEY');
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $API_KEY,
        ])->get('https://api.themoviedb.org/3/genre/movie/list?language=en');

        $data = $response->object();

        return $data;

    }

    public function getTrendingMovies()
    {
        try {
            $API_KEY = env('THEMOVIEDB_API_KEY');
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $API_KEY,
            ])->get('https://api.themoviedb.org/3/trending/all/day?language=en-US');

            $data = $response->object();

            return $data;
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }
}
