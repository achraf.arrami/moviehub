Projet Laravel/API - README
===========================

Description
-----------

Ce projet consiste en la création d'une application Laravel utilisant l'API de The Movie Database (TMDb) pour afficher les films tendances et les détails de chaque film. Le projet est dockerisé avec Laravel Sail pour une gestion facilitée des dépendances et de l'environnement de développement.

Source utilisés
----------------

-   [Flowbite](https://flowbite.com/)
-   [Select2](https://select2.org/)
-   [Logo](https://logo.com/)
-   [jQuery](https://jquery.com/)
-   [Readme.so](https://readme.so/)
-   [Jetstream](https://jetstream.laravel.com/introduction.html)
-   [Livewire](https://laravel-livewire.com/)
-   [Tailwindcss](https://tailwindcss.com/)


Installation
------------

1.  Cloner le dépôt git.
2.  Assurez-vous d'avoir Docker installé sur votre machine.
3.  Exécutez `composer install` pour installer les dépendances PHP.
4.  Exécutez `php artisan project:install` pour initialiser le projet.
5.  Ajouter api key dans le fichier .env `THEMOVIEDB_API_KEY`.
6.  Exécutez `./vendor/bin/sail up` pour démarrer les conteneurs Docker.
7.  Accédez à l'application dans votre navigateur [localhost](http://localhost/).

Accès administrateur
--------------------

**Identifiants:**

-   Email: admin@moviehub.com
-   Mot de passe: 12345678

Mises à jour automatiques
-------------------------

Le projet inclut un script artisan `php artisan movies:store-trending` qui récupère les dernières informations sur les films tendances et met à jour la base de données. Pour ajouter ce script aux tâches cron, vous devez ajouter la ligne suivante dans le cron tab :
`* * * * * cd /chemin-vers-votre-projet && php artisan schedule:run >> /dev/null 2>&1`.
Remplacez /chemin-vers-votre-projet par le chemin réel vers votre projet.

Les recherches
-------------------------
1. https://stackoverflow.com/questions/71503871/laravel-error-laravel-sail-no-such-file-or-directory-found
2. https://stackoverflow.com/questions/41906034/laravel-task-scheduler-crontab
3. https://stackoverflow.com/questions/20530996/aliases-in-windows-command-prompt
4. https://forums.docker.com/t/docker-resources-you-dont-have-any-wsl-2-distro-please-convert-a-wsl-1-distro-to-wsl-2/99100
